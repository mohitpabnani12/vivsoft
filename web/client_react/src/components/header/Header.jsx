import React from 'react'
import logo from '../assets/logo.png'
import './header.css'

export default function Header() {
    return (
        <div className="header">

            <div className="logo">
                <img className="logoImg" src={logo} alt="" />
            </div>
            <div className="headerTitle"><h2>VIVSOFT</h2></div>
            
        </div>
    )
}
