import React from 'react'
import Chart from '../chart/Chart'
import Sidebar from '../sidebar/Sidebar'
import './homepage.css'
import moment from 'moment';
import { useState } from 'react'
import { useEffect } from 'react'

export default function Homepage() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      let res = await fetch('/server/getWeatherData', {
        method: 'GET'
      });
      res = await res.json();
      const resultSet = res['forecast'].forecastday[0].hour;
      var today = Math.round((new Date()).getTime() / 1000);
      let dynamicData = [];
      dynamicData.push({
        time: 'Current',
        temp_f: res['current'].temp_f
      });
      resultSet.forEach(element => {
        if (today <= element.time_epoch) {
          dynamicData.push({
            time: moment(element.time).format("hh:mm"),
            temp_f: element.temp_f
          });
        }
      })
      setData(dynamicData);
      setTimeout(fetchData, 5000);
    }
    fetchData();
  }, [])
    return (
        <div className="homepage">

            <div className="homepageContainer">
                <div className="sidebarContainer">
                    <Sidebar />
                </div>
                <div className="chartConatiner">
                    <Chart data={data}/>
                </div>
            </div>


        </div>
    )
}
