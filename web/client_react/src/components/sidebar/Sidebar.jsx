import React from 'react'
import './sidebar.css'

export default function Sidebar() {
    return (
        <div className="sidebar">
            <ul className="sidebarMenu">
                <li className="sidebarItem">Item 1</li>
                <li className="sidebarItem">Item 2</li>
                <li className="sidebarItem">Item 3</li>
                <li className="sidebarItem">Item 4</li>
                <li className="sidebarItem">Item 5</li>
            </ul>
            
        </div>
    )
}
