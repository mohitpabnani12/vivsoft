var express = require('express');
const request = require('request');
var router = express.Router();

const url = `https://api.weatherapi.com/v1/forecast.json?key=3738897fde7047f0a1822737203011&q=20171&days=1`;

/* GET Weather Data. */
router.get('/server/getWeatherData', function(req, res, next) {
  request({ url: url, json: true }, function (error, response) { 
  if(error) {
      console.log(error);
    }
    return res.status(200).send(response.body);
  })
});

module.exports = router;
